extern crate nalgebra;

use na::DVector;
use nalgebra as na;

/// The Chebyshev interpolation and fitter functions
pub mod chebyshev;

// The Hermite interpolation and fitter functions
// pub mod hermite;

pub enum InterpErr {}

pub trait Interpolator {
    /// Compute the fit of a list of Xs and their associated Ys.
    fn fit(x: &DVector<f64>, y: &DVector<f64>, deg: usize) -> DVector<f64>;
    /// Given an x value, and a list of coefficients, this will return the function's interpolation.
    fn evaluate(x: f64, c: &[f64]) -> f64;
}
